//
//  MembersTableViewController.swift
//  Flashin Star Media
//
//  Created by Arjun Srivastava on 6/10/17.
//  Copyright © 2017 Biscuit and Chai. All rights reserved.
//

import UIKit
import AlamofireImage

class MembersTableViewController: UITableViewController {

    let members = AppController.shared.band.members
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        NotificationCenter.default.post(name: NSNotification.Name(rawValue: "PlayVideo"), object: nil)
    }


    // MARK: - Table view data source

    override func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return members.count
    }

    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        if indexPath.row%2 == 0 {
            let cell = tableView.dequeueReusableCell(withIdentifier: "member_cell", for: indexPath) as! MemberTableViewCell
            
            let member = members[indexPath.row]
            
            cell.nameLbl.text = member.name
            cell.specialtyLbl.text = member.specialty
            cell.dobLbl.text = member.dob
            cell.faveSongLbl.text = member.faveSong
            
            cell.profileImageView.af_setImage(withURL: URL(string: member.image_path)!)
            cell.profileImageView.layer.cornerRadius = 50.0
            
            return cell
        } else {
            let cell = tableView.dequeueReusableCell(withIdentifier: "member_cell_2", for: indexPath) as! MemberTableViewCell
            
            let member = members[indexPath.row]
            
            cell.nameLbl.text = member.name
            cell.specialtyLbl.text = member.specialty
            cell.dobLbl.text = member.dob
            cell.faveSongLbl.text = member.faveSong
            
            cell.profileImageView.af_setImage(withURL: URL(string: member.image_path)!)
            cell.profileImageView.layer.cornerRadius = 50.0

            return cell
        }
    }
}
