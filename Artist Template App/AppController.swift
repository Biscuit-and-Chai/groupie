//
//  AppController.swift
//  Flashin' Star Media
//
//  Created by Arjun Srivastava on 6/10/17.
//  Copyright © 2017 Biscuit and Chai. All rights reserved.
//

import Foundation
import UIKit
import SwiftyJSON

class AppController {
    static let shared = AppController(debug: true)

    var latestVideoURL: String! = "yY7iGa4t9-I"
    
    var band: Band!
    var events: [Event] = []
    var merchandise: [Merchandise] = []
    
    init() { }
    
    init(debug: Bool) {
        if let path = Bundle.main.path(forResource:"test", ofType: "json") {
            do {
                
                let jsonData = try Data(contentsOf: URL(fileURLWithPath:path), options: .mappedIfSafe)
                
                let json = JSON(data: jsonData)
                
                if let info = json.dictionaryObject {
                    self.band = Band(bandInfo: info["band"] as! [String : Any])
                    
                    for event in info["events"] as! [[String : Any]] {
                        self.events += [Event(event)]
                    }
                    
                    for merchandise in info["merchandise"] as! [[String : Any]] {
                        self.merchandise += [Merchandise(merchandise)]
                    }
                }
                
            } catch {
                print("\(error)")
            }
            
        }
        
    }
}
