//
//  ViewController.swift
//  Flashin' Star Media
//
//  Created by Arjun Srivastava on 6/9/17.
//  Copyright © 2017 Biscuit and Chai. All rights reserved.
//

import UIKit
import AVFoundation

class ViewController: UIViewController, YouTubePlayerDelegate, UICollectionViewDelegate, UICollectionViewDataSource {
    
    @IBOutlet weak var sampleImageVideo: UIImageView!
    
    @IBOutlet var youtubePlayerView: YouTubePlayerView!

    @IBOutlet weak var eventsCollectionView: UICollectionView!
    
    @IBOutlet weak var loadingPlayer: UIActivityIndicatorView!
    
    @IBOutlet weak var volumeButton: UIButton!
    
    let events = AppController.shared.events
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        youtubePlayerView.delegate = self
        
        NotificationCenter.default.addObserver(self, selector: #selector(ViewController.endedVideo), name: NSNotification.Name(rawValue: "EndState"), object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(ViewController.playVideo), name: NSNotification.Name(rawValue: "PlayVideo"), object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(ViewController.pauseVideo), name: NSNotification.Name(rawValue: "PauseVideo"), object: nil)

        youtubePlayerView.loadVideoID(AppController.shared.latestVideoURL)

    }

    //MARK: - YT Player View
    func playerReady(_ videoPlayer: YouTubePlayerView) {
        navigationItem.leftBarButtonItem?.isEnabled = true
        
        youtubePlayerView.play()
    }
    func playerStateChanged(_ videoPlayer: YouTubePlayerView, playerState: YouTubePlayerState) {
        if playerState == .Playing {
            loadingPlayer.stopAnimating()
            volumeButton.isHidden = false
        }
    }
    func endedVideo(){
    }
    func playVideo(){
        youtubePlayerView.play()
    }
    func pauseVideo() {
        youtubePlayerView.pause()
    }
    
    
    //MARK: - Events
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return events.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "event_cell", for: indexPath) as! EventCollectionViewCell
        
        let event = events[indexPath.row]
        
        cell.eventNameLbl.text = event.name
        cell.eventDateLbl.text = event.date
        cell.eventImageView.af_setImage(withURL: URL(string: event.imagePath)!)
        
        cell.purchaseTixBtn.isEnabled = !event.isExpired
        
        return cell
    }
    
    @IBAction func toggleVolume(_ sender: Any) {
        if (youtubePlayerView.playerState == .Playing) {
            youtubePlayerView.pause()
            volumeButton.setImage(#imageLiteral(resourceName: "mute-icon"), for: .normal)

        } else {
            youtubePlayerView.play()
            volumeButton.setImage(#imageLiteral(resourceName: "volume-icon"), for: .normal)
        }
    }
}

