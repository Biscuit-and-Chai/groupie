////
////  LiveStream.swift
////  Artist Template App
////
////  Created by Arjun Srivastava on 10/18/17.
////  Copyright © 2017 Biscuit and Chai. All rights reserved.
////
//
//import Foundation
//
//struct Config {
//    
//    static var rtmpPushUrl = "rtmp://106.187.100.229/live/"
//    static var rtmpPlayUrl = "rtmp://106.187.100.229/live/"
//    static var serverUrl = "http://192.168.100.70:3000"
//    
//}
//
//struct Room {
//    
//    var key: String
//    var title: String
//    
//    init(dict: [String: AnyObject]) {
//        title = dict["title"] as! String
//        key = dict["key"] as! String
//    }
//    
//    func toDict() -> [String: AnyObject] {
//        return [
//            "title": title as AnyObject,
//            "key": key as AnyObject
//        ]
//    }
//}
//
//
//struct Comment {
//    
//    var text: String
//    
//    init(dict: [String: AnyObject]) {
//        text = dict["text"] as! String
//    }
//}
//
//
//struct User {
//    
//    var id = Int(arc4random())
//    
//    static let currentUser = User()
//}
//
//
//class GiftEvent: NSObject {
//    
//    var senderId: Int
//    
//    var giftId: Int
//    
//    var giftCount: Int
//    
//    init(dict: [String: AnyObject]) {
//        senderId = dict["senderId"] as! Int
//        giftId = dict["giftId"] as! Int
//        giftCount = dict["giftCount"] as! Int
//    }
//    
//    func shouldComboWith(_ event: GiftEvent) -> Bool {
//        return senderId == event.senderId && giftId == event.giftId
//    }
//    
//}
//
