//
//  Band.swift
//  Flashin' Star Media
//
//  Created by Arjun Srivastava on 6/10/17.
//  Copyright © 2017 Biscuit and Chai. All rights reserved.
//

import Foundation

class Event {
    var name: String!
    var date: String!
    var details: String!
    var imagePath: String!
    
    var isExpired: Bool!
    
    var photoPaths: [String] = [] //only exists for expired/past events

    init(_ eventInfo: [String : Any]) {
        self.name = eventInfo["name"] as! String
        self.date = eventInfo["date"] as! String
        self.details = eventInfo["details"] as! String
        self.imagePath = eventInfo["image_path"] as! String
        self.isExpired = eventInfo["is_expired"] as! Bool
        
        if isExpired {
            for photo in eventInfo["photos"] as! [String] {
                self.photoPaths += [photo]
            }
        }
    }
}

class Member {
    var name: String!
    var dob: String!
    var specialty: String!
    var faveSong: String!
    var image_path: String!
    var moreInfo: [String: String] = [:]
    
    init(_ memberInfo: [String : Any]) {
        self.name = memberInfo["name"] as! String
        self.dob = memberInfo["birthday"] as! String
        self.specialty = memberInfo["specialty"] as! String
        self.faveSong = memberInfo["fave_song"] as! String
        self.image_path = memberInfo["image_path"] as! String
    }
}

class Merchandise {
    var name: String!
    var details: String!
    var price: String!
    var sizes: [String] = []
    var imagePath: String!
    
    init(_ merchandise: [String : Any]) {
        self.name = merchandise["item_name"] as! String
        self.details = merchandise["details"] as! String
        self.price = merchandise["price"] as! String
        self.imagePath = merchandise["image_path"] as! String
        self.sizes = merchandise["sizes"] as! [String]
    }
}

class Band {
    var name: String!
    var image_path: String!
    var details: String!
    var members: [Member] = []
    
    init(bandInfo: [String : Any]) {
        self.name = bandInfo["artist_name"] as! String
        self.image_path = bandInfo["artist_logo_path"] as! String
        self.details = bandInfo["artist_details"] as! String
    
        for memberInfo in bandInfo["members"] as! [[String : Any]] {
            self.members += [Member(memberInfo)]
        }
    }
}
