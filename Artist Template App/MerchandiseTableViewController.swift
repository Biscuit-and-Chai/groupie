//
//  MerchandiseTableViewController.swift
//  Artist Template App
//
//  Created by Arjun Srivastava on 6/10/17.
//  Copyright © 2017 Biscuit and Chai. All rights reserved.
//

import UIKit
import AlamofireImage

class MerchandiseTableViewController: UITableViewController {

    let merchandise = AppController.shared.merchandise
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    // MARK: - Table view data source

    override func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return merchandise.count
    }

    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "merch_cell", for: indexPath) as! MerchandiseTableViewCell
        
        let merch = merchandise[indexPath.row]
        
        cell.nameLbl.text = merch.name
        cell.detailsLbl.text = merch.details
        cell.priceLbl.text = merch.price
        
        //NEED TO REMOVE APP TRANSPORT SETTINGS FROM INFO.PLIST - ONLY USING IT CUZ TEST IMAGE HAS HTTP:// RATHER THAN HTTPS://
        cell.itemImageView.af_setImage(withURL: URL(string:merch.imagePath)!)
        cell.itemImageView.layer.cornerRadius = 5.0
        
        cell.smallBtn.isEnabled = merch.sizes.contains("s")
        cell.mediumBtn.isEnabled = merch.sizes.contains("m")
        cell.largeBtn.isEnabled = merch.sizes.contains("l")
        cell.xlargeBtn.isEnabled = merch.sizes.contains("xl")
        
        if cell.smallBtn.isEnabled == false && cell.mediumBtn.isEnabled == false && cell.largeBtn.isEnabled == false && cell.xlargeBtn.isEnabled == false {
            cell.addToCartBtn.isEnabled = false
            cell.buyNowBtn.isEnabled = false
        }
        
        return cell
    }
}
