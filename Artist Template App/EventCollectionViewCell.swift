//
//  EventCollectionViewCell.swift
//  Artist Template App
//
//  Created by Arjun Srivastava on 6/10/17.
//  Copyright © 2017 Biscuit and Chai. All rights reserved.
//

import UIKit

class EventCollectionViewCell: UICollectionViewCell {
    
    @IBOutlet weak var eventNameLbl: UILabel!
    @IBOutlet weak var eventDateLbl: UILabel!
    @IBOutlet weak var purchaseTixBtn: UIButton!
    @IBOutlet weak var eventImageView: UIImageView!
    
}
